<?php declare(strict_types=1);
final class Email
{
    private $email;
    private $notUsed;
    private function __construct(string $email)
    {
        $this->ensureIsValidEmail($email);

        $this->email = $email;
    }

    public static function fromString(string $email): self
    {

        return new self($email);
    }

    public static function nonCovered(): void
    {
        //nothing to do
        $test = 12;
        $res = $test / 0;
    }

    public function __toString(): string
    {
        return $this->email;
    }

    private function ensureIsValidEmail(string $email): void
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException(
                sprintf(
                    '"%s" is not a valid email address',
                    $email
                )
            );
        }
    }
}